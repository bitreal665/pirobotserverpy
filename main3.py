from br_launcher import light_launcher
import time


def main():
    __launcher = light_launcher.LightLauncher()
    __launcher.start()

if __name__ == "__main__":
    time.sleep(5)
    main()
