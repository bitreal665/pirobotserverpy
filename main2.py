from br_robot import robot_service


def main():
    __robot = robot_service.RobotService()
    __robot.start()

if __name__ == "__main__":
    main()
