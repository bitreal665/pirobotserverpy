from . import logger


class CompositeLogger(logger.Logger):

    def __init__(self, loggers):
        self.__loggers = loggers

    def init(self):
        for l in self.__loggers:
            if not l.init():
                return False
        return True

    def log(self, lvl, tag, msg):
        for l in self.__loggers:
            l.log(lvl, tag, msg)

    def deinit(self):
        for l in self.__loggers[::-1]:
            l.deinit()
