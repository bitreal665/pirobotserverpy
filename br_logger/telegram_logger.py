from . import base_logger


class TelegramLogger(base_logger.BaseLogger):

    def __init__(self, bot, settings):
        base_logger.BaseLogger.__init__(self, settings)
        self.__bot = bot

    def _format_msg(self, lvl, tag, msg):
        log_msg = base_logger.BaseLogger._format_msg(self, lvl, tag, msg)
        return log_msg + " \U0001F4AC"

    def _log(self, lvl, tag, msg):
        log_msg = self._format_msg(lvl, tag, msg)
        self.__bot.send_msg(log_msg, self._settings["chat_id"])
