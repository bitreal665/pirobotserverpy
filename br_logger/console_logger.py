import traceback
from . import base_logger


class ConsoleColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def log2console(msg):
    if isinstance(msg, Exception):
        msg = traceback.format_exc()
    print(ConsoleColors.HEADER + str(msg))


class ConsoleLogger(base_logger.BaseLogger):

    def __init__(self, settings):
        base_logger.BaseLogger.__init__(self, settings)

    def _format_msg(self, lvl, tag, msg):
        log_msg = base_logger.BaseLogger._format_msg(self, lvl, tag, msg)
        if lvl[1] == 1:
            color = ConsoleColors.OKBLUE
        elif lvl[1] == 2:
            color = ConsoleColors.WARNING
        elif lvl[1] == 3:
            color = ConsoleColors.FAIL
        else:
            color = ConsoleColors.ENDC
        log_msg = color + log_msg
        return log_msg

    def _log(self, lvl, tag, msg):
        log_msg = self._format_msg(lvl, tag, msg)
        print(log_msg)
