import os
from time import gmtime, strftime
from . import console_logger
from . import base_logger


class FileLogger(base_logger.BaseLogger):

    def __init__(self, settings):
        base_logger.BaseLogger.__init__(self, settings)
        self.__file = None
        self.__lines = 0
        self.__last_flushed = 0

    def init(self):
        try:
            datetime = strftime("%d_%m_%Y__%H_%M_%S", gmtime())
            file_name = "log_%s.log" % datetime
            if self._settings['dir'] and self._settings['dir'] != '':
                if not os.path.exists(self._settings['dir']):
                    os.makedirs(self._settings['dir'])
                file_name = self._settings['dir'] + "/" + file_name
            self.__file = open(file_name, "w", encoding='utf-8')
        except Exception as e:
            self.__file = None
            console_logger.log2console(e)
            return False

        self.__lines = 0
        self.__last_flushed = 0

        return True

    def _log(self, lvl, tag, msg):
        log_msg = self._format_msg(lvl, tag, msg)
        if self.__file:
            if self.__lines < 100000:
                try:
                    self.__file.write(log_msg + '\n')
                except Exception as e:
                    console_logger.log2console('write in log file error')
                    console_logger.log2console(e)
                self.__lines += 1
            else:
                console_logger.log2console("too many lines")

            if self.__lines - self.__last_flushed >= self._settings['flush_frequency'] or lvl[1] >= self._settings['min_flash_level']:
                try:
                    self.__file.flush()
                    self.__last_flushed = self.__lines
                except Exception as e:
                    console_logger.log2console('flush log file error')
                    console_logger.log2console(e)

    def deinit(self):
        if self.__file:
            self.__file.close()
            self.__file = None
