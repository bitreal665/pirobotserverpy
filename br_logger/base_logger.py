import traceback
from time import gmtime, strftime
from . import logger


class BaseLogger(logger.Logger):

    def __init__(self, settings):
        self._settings = settings

    def log(self, lvl, tag, msg):
        if self._is_pass_msg(lvl, tag, msg):
            self._log(lvl, tag, msg)

    def _is_pass_msg(self, lvl, tag, msg):
        if lvl[1] < self._settings['min_level']:
            return False
        if len(self._settings['tags']) > 0 and tag not in self._settings['tags']:
            return False
        return True

    def _format_msg(self, lvl, tag, msg):
        time = strftime("%H:%M:%S", gmtime())
        log_msg = "[%s][%s][%s] %s" % (time, lvl[0], tag, str(msg))
        if isinstance(msg, Exception):
            log_msg = traceback.format_exc()
        return log_msg

    def _log(self, lvl, tag, msg):
        pass
