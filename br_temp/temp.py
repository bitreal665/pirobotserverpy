import socket
import time


def test1():
    msg_list = [
        '',
        '\0',
        ';',
        ')(',
        ':',
        ':(',
        ':()',
        ':();',
        ':();;;;',
        'asd:\0asd',
        '0:OFF();',
    ]
    for msg in msg_list:
        sock = socket.socket()
        sock.connect(('localhost', 12345))
        b = bytearray(msg, 'utf-8')
        sock.send(b)
        sock.close()
        time.sleep(1)


def test2():
    sock = socket.socket()
    sock.connect(('localhost', 12345))
    sock.close()


def test3():
    sock = socket.socket()
    sock.connect(('localhost', 12345))


def test4():
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 12345))
    sock.listen(1)
    print('open')

    conn, addr = sock.accept()
    print('accept')

    while True:
        try:
            req = conn.recv(1024)
            if len(req) <= 0:
                print("recv 0")
                break
            req_str = req.decode("utf-8")
            print("%s %d" % (req_str, len(req)))
            resp = req
            if conn.send(resp) <= 0:
                print("send 0")
                break
        except Exception as e:
            print(str(e))
            break

    conn.close()
    print('close')

    sock.close()
    print('close')


test4()
