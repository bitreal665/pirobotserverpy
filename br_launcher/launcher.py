from time import gmtime, strftime
from br_logger import *
from br_robot import *
from br_telegram import *
from . import config


class Launcher:

    help_msg = '''
Commands:
/start - start command
/help - supported commands list
/start_robot - start robot server
/get_info - get info (robot server status, startup time, ...)
/stop_robot - stop robot server
'''

    info_msg = '''
startup time: {startup_time}
server status: {robot_status}
server startup time: {robot_startup_time}
'''

    start_msg = '''
Hi! I'm Cat's Nanny Robot server launcher.

Use /help for help \U0001F643'''

    __tag = "LAUNCHER"

    def __init__(self):
        self.__logger = composite_logger.CompositeLogger([
            console_logger.ConsoleLogger(config.logger["console"]),
            file_logger.FileLogger(config.logger["file"])
        ])
        self.__bot = telegram_bot.TelegramBot(config.telegram["token"], self.__logger)
        self.__robot = None
        self.__startup_time = None

    def init(self):
        console_logger.log2console("launcher initing...")
        if not self.__logger.init():
            console_logger.log2console("launcher not inited (logger not inited)")
            exit(-1)
        self.__bot.start_send_thread()
        self.__startup_time = gmtime()
        self.__logger.log(logger.I, self.__tag, "inited")
        self.__bot.send_msg("Hello world!", config.telegram["chat_id"])
        self.__bot.start_update(self.__update_callback)

    def __update_callback(self, msg):
        if msg["text"] == "/start":
            self.__start(msg)
        elif msg["text"] == "/help":
            self.__help(msg)
        elif msg["text"] == "/start_robot":
            self.__start_robot(msg)
        elif msg["text"] == "/get_info":
            self.__get_info(msg)
        elif msg["text"] == "/stop_robot":
            self.__stop_robot(msg)
        else:
            self.__bot.replay("bad cmd! use /help", msg)

    def __start(self, msg):
        self.__bot.replay(Launcher.start_msg, msg)

    def __help(self, msg):
        self.__bot.replay(Launcher.help_msg, msg)

    def __start_robot(self, msg):
        if self.__robot:
            self.__bot.replay("robot already launched", msg)
            return
        self.__bot.replay("starting...", msg)
        if self._start_robot():
            self.__bot.replay("started", msg)
        else:
            self.__bot.replay("NOT started", msg)

    def __get_info(self, msg):
        info_str = Launcher.info_msg.format(
            startup_time=strftime("%H:%M:%S", self.__startup_time),
            robot_status="online \U00002705" if self.__robot else "offline \U00002611",
            robot_startup_time=strftime("%H:%M:%S", self.__robot.startup_time) if self.__robot and self.__robot.startup_time else "None",
        )
        self.__bot.replay(info_str, msg)

    def __stop_robot(self, msg):
        if not self.__robot:
            self.__bot.replay("robot not launched", msg)
            return
        self.__bot.replay("stopped...", msg)
        self._stop_robot()
        self.__bot.replay("stopped", msg)

    def _start_robot(self):
        self.__logger.log(logger.I, self.__tag, "robot starting...")
        try:
            self.__robot = robot_service.RobotService(self.__bot)
            if self.__robot.init():
                self.__logger.log(logger.I, self.__tag, "robot started")
                return True
            else:
                self.__robot = None
                self.__logger.log(logger.W, self.__tag, "robot starting error")
                return False
        except Exception as e:
                self.__robot = None
                self.__logger.log(logger.W, self.__tag, "robot starting exception:")
                self.__logger.log(logger.W, self.__tag, e)
                return False

    def _stop_robot(self):
        self.__logger.log(logger.I, self.__tag, "robot stopping...")
        try:
            self.__robot.deinit()
            self.__robot = None
            self.__logger.log(logger.I, self.__tag, "robot stopped...")
        except Exception as e:
            self.__robot = None
            self.__logger.log(logger.W, self.__tag, "robot stopping exception:")
            self.__logger.log(logger.W, self.__tag, e)
