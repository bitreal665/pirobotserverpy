from br_logger import *
from br_robot import *
from br_telegram import *
from . import config
from base_app import BaseApp
import time


class LightLauncher(BaseApp):

    __tag = 'LAUNCHER'

    help_msg = '''
Commands:
/help - supported commands list
/start_robot - start robot
/stop_launcher - stop launcher

video: http://109.167.141.224:8000/stream.mjpg
'''

    def __init__(self):
        super(LightLauncher, self).__init__(LightLauncher.__tag, config.logger)
        self.__bot = telegram_bot.TelegramBot(config.telegram["token"], self._logger)
        self._app_components.append(self.__bot)

    def _on_main_loop(self):
        # пропускаем все комманды за время оффлайна
        self.__bot.update_sync()

        self.__bot.send_msg_sync("Hello world!", config.telegram["chat_id"])

        while True:
            messages = self.__bot.update_sync()
            if messages is None:
                self._logger.log(logger.W, self.__tag, "messages is None")
                time.sleep(60)
                continue
            if len(messages) <= 0:
                time.sleep(5)
                continue
            msg = messages[-1]
            if msg['text'] == '/help':
                self.__bot.replay_sync(LightLauncher.help_msg, msg)
            elif msg['text'] == '/start_robot':
                self.__bot.replay_sync('starting...', msg)
                try:
                    robot = robot_service.RobotService()
                    robot.start()
                except Exception as e:
                    self._logger.log(logger.E, self.__tag, "robot exception:")
                    self._logger.log(logger.E, self.__tag, e)
                self.__bot.replay_sync('stoped', msg)
            elif msg['text'] == '/stop_launcher':
                break
            else:
                self.__bot.replay_sync('bad cmd! use /help', msg)

        self.__bot.send_msg_sync("Good buy world!", config.telegram["chat_id"])
