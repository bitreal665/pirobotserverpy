import http.client
import json
import threading
import time
from br_logger import logger


class TelegramBot:

    url = "api.telegram.org"

    __tag = "TELEGRAM_BOT"

    def __init__(self, token, a_logger):
        self.__logger = a_logger
        self.__token = token
        self.__sending = False
        self.__send_thread = None
        self.__send_pool = []
        self.__updating = False
        self.__update_id = 1

    def init(self):
        return True

    def deinit(self):
        pass

    def start_send_thread(self):
        if self.__send_thread:
            return
        self.__logger.log(logger.I, self.__tag, "send thread starting...")
        self.__sending = True
        self.__send_thread = threading.Thread(target=self.__send_loop)
        self.__send_thread.start()
        self.__logger.log(logger.I, self.__tag, "send thread started")

    def stop_send_thread(self):
        if not self.__send_thread:
            return
        self.__logger.log(logger.I, self.__tag, "send thread stopping...")
        self.__sending = False
        self.__send_thread.join()
        self.__send_thread = None
        self.__logger.log(logger.I, self.__tag, "send thread stopped")

    def replay(self, text, msg):
        self.send_msg(text, msg["chat"]["id"])

    def replay_sync(self, text, msg):
        self.send_msg_sync(text, msg["chat"]["id"])

    def send_msg(self, text, chat_id):
        self.__send_pool.append(self.__create_msg(text, chat_id))

    def send_msg_sync(self, text, chat_id):
        msg = self.__create_msg(text, chat_id)
        self.__send(msg["method"], msg["data"], logger.I)

    def __create_msg(self, text, chat_id):
        return {"method": "sendMessage", "data": {"text": text, "chat_id": chat_id}, "callback": None}

    def __send_loop(self):
        while self.__sending:
            log_level = logger.I if len(self.__send_pool) > 0 else logger.D
            self.__logger.log(log_level, self.__tag, "sending pool size=%s" % len(self.__send_pool))
            while len(self.__send_pool) > 0:
                args = self.__send_pool[0]
                resp = self.__send(args["method"], args["data"], logger.I)
                if not resp:
                    break
                self.__send_pool.pop(0)
                try:
                    if args["callback"]:
                        args["callback"](resp)
                except Exception as e:
                    self.__logger.log(logger.W, self.__tag, "exception in sending callback:")
                    self.__logger.log(logger.W, self.__tag, e)
            time.sleep(1)

    def start_update(self, callback):
        self.__updating = True
        self.__logger.log(logger.I, self.__tag, "start update loop")
        while self.__updating:
            messages = self.update_sync()
            for msg in messages:
                try:
                    callback(msg)
                except Exception as e:
                    self.__logger.log(logger.W, self.__tag, "exception in update callback:")
                    self.__logger.log(logger.W, self.__tag, e)
            time.sleep(2)
        self.__logger.log(logger.I, self.__tag, "stop update loop")

    def stop_update(self):
        self.__updating = False

    def update_sync(self):
        resp = self.__send("getUpdates", {"offset": self.__update_id}, logger.D)
        if resp is None:
            return None
        log_level = logger.I if len(resp["result"]) > 0 else logger.D
        self.__logger.log(log_level, self.__tag, "new updates=%s" % len(resp["result"]))
        self.__logger.log(log_level, self.__tag, "updates=%s" % str(resp))
        messages = []
        for item in resp["result"]:
            self.__update_id = int(item["update_id"]) + 1
            messages.append(item["message"])
        return messages

    def __send(self, method, data, log_level):
        self.__logger.log(log_level, self.__tag, "start sending method=%s data=%s" % (method, str(data)))
        path = "/bot%s/%s" % (self.__token, method)
        try:
            conn = http.client.HTTPSConnection(TelegramBot.url)
            conn.request("POST", path, json.dumps(data), {"Content-Type": "application/json"})
            resp = conn.getresponse()
        except Exception as e:
            self.__logger.log(logger.W, self.__tag, "sending exception:")
            self.__logger.log(logger.W, self.__tag, e)
            return None
        if resp.code != 200:
            self.__logger.log(logger.W, self.__tag, "sending: bad response code:%s" % resp.code)
            return None
        try:
            resp_bytes = resp.read()
        except Exception as e:
            self.__logger.log(logger.W, self.__tag, "sending: answer reading exception:")
            self.__logger.log(logger.W, self.__tag, e)
            return None
        resp_str = resp_bytes.decode("utf-8")  # todo: kramarov: с хрена ли utf-8? так-то нужно смотреть хидер
        self.__logger.log(log_level, self.__tag, "sending: answer=%s" % resp_str)
        resp_json = json.loads(resp_str)
        if not resp_json:
            self.__logger.log(logger.W, self.__tag, "sending: answer is not json")
            return None
        if "ok" not in resp_json:
            self.__logger.log(logger.W, self.__tag, "sending: answer hasn't 'ok' field")
            return None
        if not resp_json["ok"]:
            self.__logger.log(logger.W, self.__tag, "sending: answer's 'ok' field is False")
            return None
        return resp_json
