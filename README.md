I tried to develop a robot on RaspberryPI that should have looked after my cat. I should have been able to:

- move by chassis

- spy on a cat by web camera

- give food to a cat

- clean cat's toilet

But I implemented only the first two points :)

Robot's software is a client application for Android and a server for the robot that listens and executes commands from the client.

The client applcation is a regular Android Java application that sends commands to the server and displays video from robot's web camera. The client's code is here - https://bitbucket.org/bitreal665/pirobotclient/src/master/

The server is written in Python. Server has 2 parts. The first one is a robot service that listens and executes commands from the client. The second one is a launcher for the robot service. The launcher is a Telegram Bot that gives me an easy way to start and stop the robot service. So the launcher is a light service that always works. The robot service runs on demand by the launcher. A repository with server code is here https://bitbucket.org/bitreal665/pirobotserverpy/src/master/

![](ImagesForReadme/piRobot.JPG)

There is a short video with the robot and my cat https://youtu.be/OMktrkMD09M  ^_^