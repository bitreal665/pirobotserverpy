from br_launcher import launcher
from br_logger import console_logger


def main():
    console_logger.log2console("main")
    l = launcher.Launcher()
    l.init()


if __name__ == "__main__":
    main()
