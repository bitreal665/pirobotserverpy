from br_logger import logger


class CmdParser:

    __tag = "CMD_PRSR"

    def __init__(self, a_logger):
        self.__logger = a_logger

    def parse(self, cmd_str):

        if cmd_str is None:
            self.__logger.log(logger.W, self.__tag, "cmd_str is None")
            return None

        if cmd_str == '':
            self.__logger.log(logger.W, self.__tag, "cmd_str is empty")
            return None

        cmd_str = cmd_str.split(" ")

        cmd = {}
        cmd['name'] = cmd_str[0]
        if len(cmd_str) > 1:
            cmd['args'] = cmd_str[1:]
        else:
            cmd['args'] = []

        return cmd
