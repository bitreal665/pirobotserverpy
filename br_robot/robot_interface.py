from br_logger import logger


class RobotInterface:

    __tag = "ROB_INT"

    def __init__(self, a_logger, a_camera_service, a_gpio_controller, a_life_cycle):
        self.__logger = a_logger
        self.__camera_service = a_camera_service
        self.__gpio_controller = a_gpio_controller
        self.__life_cycle = a_life_cycle

    def init(self):
        self.__logger.log(logger.I, self.__tag, "initing...")
        self.__logger.log(logger.I, self.__tag, "inited")
        return True

    def enable_camera(self, is_enable):
        if is_enable != 0:
            self.__camera_service.start()
        else:
            self.__camera_service.stop()
        return 0, ''

    def drive(self, l_speed, r_speed):
        self.__gpio_controller.drive(l_speed, r_speed)
        return 0, ''

    def rotate_camera(self, angle):
        self.__gpio_controller.rotate_camera(angle)
        return 0, ''

    def exit(self):
        self.__life_cycle.exit()
        return 0, ''

    def deinit(self):
        self.__logger.log(logger.I, self.__tag, "deiniting...")
        self.__logger.log(logger.I, self.__tag, "deinited")
