class PiCamera:

    def __init__(self):
        self.hflip = False
        self.vflip = False
        self.framerate = 0
        self.resolution = ()

    def start_recording(self, output, format, quality):
        pass

    def stop_recording(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
