import io
import socketserver
import threading
from http import server
from threading import Condition

#import picamera
from . import camera_stub as picamera
from br_logger import logger
from . import config


class StreamingOutput(object):

    def __init__(self):
        self.frame = None
        self.buffer = None
        self.condition = None

    def init(self):
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

    def deinit(self):
        self.condition = None
        self.buffer.close()
        self.buffer = None
        self.frame = None


class StreamingHandler(server.BaseHTTPRequestHandler):

    PAGE = """\
<html>
<head>
<title>picamera MJPEG streaming demo</title>
</head>
<body>
<h1>PiCamera MJPEG Streaming Demo</h1>
<img src="stream.mjpg" width="640" height="480" />
</body>
</html>
"""

    __tag = "CAMERA_STREAMING_HANDLER"

    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = StreamingHandler.PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with CameraService.output.condition:
                        CameraService.output.condition.wait()
                        frame = CameraService.output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                CameraService.a_logger.log(logger.W, self.__tag, "exception:")
                CameraService.a_logger.log(logger.W, self.__tag, e)
        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True


class CameraService:

    # todo: kramarov: не знаю, как прокинуть зависимость в handler
    output = None
    a_logger = None

    __tag = "CAMERA_SERVICE"

    def __init__(self, a_logger):
        self.__logger = a_logger
        self.__server = None
        self.__thread = None
        pass

    def init(self):
        self.__logger.log(logger.I, self.__tag, "initing...")
        CameraService.a_logger = logger
        self.__logger.log(logger.I, self.__tag, "inited")
        return True

    def __job(self):
        self.__logger.log(logger.I, self.__tag, "loop started")
        with picamera.PiCamera() as camera:
            CameraService.output = StreamingOutput()
            CameraService.output.init()
            camera.hflip = True
            camera.vflip = True
            camera.framerate = config.camera['frame_rate']
            camera.resolution = config.camera['resolution']
            camera.start_recording(CameraService.output, format='mjpeg', quality=config.camera['quality'])
            try:
                address = ('', config.camera['port'])
                self.__server = StreamingServer(address, StreamingHandler)
                self.__server.serve_forever()
                self.__server.server_close()
            except Exception as e:
                self.__logger.log(logger.E, self.__tag, "exception in camera:")
                self.__logger.log(logger.E, self.__tag, e)
            self.__server = None
            camera.stop_recording()
            CameraService.output.deinit()
            CameraService.output = None
        self.__logger.log(logger.I, self.__tag, "loop stopped")

    def start(self):
        if self.__thread is not None:
            self.__logger.log(logger.W, self.__tag, "already started")
            return
        self.__thread = threading.Thread(target=self.__job)
        self.__thread.start()

    def stop(self):
        if self.__thread is None:
            self.__logger.log(logger.W, self.__tag, "still not started")
            return
        if self.__server:
            self.__server.shutdown()
        self.__thread.join()
        self.__thread = None

    def deinit(self):
        if self.__thread is not None:
            self.stop()
        self.__logger.log(logger.I, self.__tag, "deiniting...")
        CameraService.a_logger = None
        self.__logger.log(logger.I, self.__tag, "deinited")



