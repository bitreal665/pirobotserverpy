from br_logger import logger


class LifeCycle:

    __tag = "LF_CLC"

    def __init__(self, a_logger):
        self.__logger = a_logger
        self.__is_exit = False

    def init(self):
        self.__logger.log(logger.I, self.__tag, "initing...")
        self.__logger.log(logger.I, self.__tag, "inited")
        self.__is_exit = False
        return True

    def exit(self):
        self.__is_exit = True
        return True

    def is_exit(self):
        return self.__is_exit

    def deinit(self):
        self.__logger.log(logger.I, self.__tag, "deiniting...")
        self.__logger.log(logger.I, self.__tag, "deinited")
