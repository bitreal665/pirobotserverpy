#import RPi.GPIO as GPIO
from . import gpio_stub as GPIO
from br_logger import logger
from . import config


class GPIOController:

    __tag = "GPIO"

    def __init__(self, a_logger):
        self.__logger = a_logger
        self.__camera_pwm = None

    def init(self):
        self.__logger.log(logger.I, self.__tag, "initing...")
        GPIO.setmode(GPIO.BCM)
        # left
        GPIO.setup(config.gpio['left']['speed_pin'], GPIO.OUT)
        GPIO.setup(config.gpio['left']['forward_pin'], GPIO.OUT)
        GPIO.setup(config.gpio['left']['back_pin'], GPIO.OUT)
        # right
        GPIO.setup(config.gpio['right']['speed_pin'], GPIO.OUT)
        GPIO.setup(config.gpio['right']['forward_pin'], GPIO.OUT)
        GPIO.setup(config.gpio['right']['back_pin'], GPIO.OUT)
        # camera
        GPIO.setup(config.gpio['camera']['pin'], GPIO.OUT)
        self.__camera_pwm = GPIO.PWM(config.gpio['camera']['pin'], 100)
        self.__camera_pwm.start(0)
        self.__logger.log(logger.I, self.__tag, "inited")
        return True

    def rotate_camera(self, angle):
        self.__logger.log(logger.D, self.__tag, "rotate_camera(%s)" % angle)
        self.__camera_pwm.ChangeDutyCycle(max(0, min(round((angle + 1.) * 10.), 20)))  # -1 ... 1 => 0 ... 20
        return True

    def drive(self, l_speed, r_speed):
        self.__logger.log(logger.D, self.__tag, "drive(%s,%s)" % (l_speed, r_speed))
        left = config.gpio['left']
        self.__drive(l_speed, left['speed_pin'], left['forward_pin'], left['back_pin'])
        right = config.gpio['right']
        self.__drive(r_speed, right['speed_pin'], right['forward_pin'], right['back_pin'])
        return True

    def __drive(self, speed, speed_pin, forward_pin, back_pin):
        if abs(speed) < 0.1:
            GPIO.output(speed_pin, GPIO.LOW)
            GPIO.output(forward_pin, GPIO.LOW)
            GPIO.output(back_pin, GPIO.LOW)
        elif speed > 0:
            GPIO.output(speed_pin, GPIO.HIGH)
            GPIO.output(forward_pin, GPIO.HIGH)
            GPIO.output(back_pin, GPIO.LOW)
        else:
            GPIO.output(speed_pin, GPIO.HIGH)
            GPIO.output(forward_pin, GPIO.LOW)
            GPIO.output(back_pin, GPIO.HIGH)

    def deinit(self):
        self.__logger.log(logger.I, self.__tag, "deiniting...")
        self.__camera_pwm.stop()
        self.__camera_pwm = None
        GPIO.cleanup()
        self.__logger.log(logger.I, self.__tag, "deinited")
