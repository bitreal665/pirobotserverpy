from unittest import TestCase
from br_logger import logger
from br_robot import cmd_parser


class TestCmdParser(TestCase):

    @classmethod
    def setUpClass(cls):
        cls._logger = logger.Logger()
        cls._parser = cmd_parser.CmdParser(cls._logger)
        cls._logger.init()

    def test_parse(self):
        _cmd_str_list = [
            ('', []),
            ('\0', []),
            (';', []),
            (':;', []),
            (':);', []),
            (':(;', []),
            ('(:;', []),
            ('(:;;;;', []),
            ('0:exit()', []),
            ('0:exit();', [{'id': '0', 'name': 'exit', 'args': []}]),
            ('0:exit(a);', [{'id': '0', 'name': 'exit', 'args': ['a']}]),
            ('0:exit(a, 123);', [{'id': '0', 'name': 'exit', 'args': ['a', ' 123']}]),

            ('0:n1();1:n2(a1,a2);', [{'id': '0', 'name': 'n1', 'args': []}, {'id': '1', 'name': 'n2', 'args': ['a1', 'a2']}]),
        ]
        for _cmd_str in _cmd_str_list:
            cmd_list = self._parser.parse(_cmd_str[0])
            with self.subTest(i=_cmd_str[0]):
                self.assertEqual(cmd_list, _cmd_str[1])

    @classmethod
    def tearDownClass(cls):
        cls._logger.deinit()
        cls._parser = None
        cls._logger = None
