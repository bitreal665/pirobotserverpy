logger = {
    'console': {
        'min_level': 0,
        'tags': [],
    },
    'file': {
        'min_level': 1,
        'tags': [],
        'dir': '_robot_logs',
        'flush_frequency': 100,
        'min_flash_level': 2,
    },
}

net = {
    'port': 12345,
    'in': {
        'clip_last_char': False,
    },
    'out': {
        'add_cr': False,
    },
}

gpio = {
    'left': {
        'speed_pin': 4,
        'forward_pin': 17,
        'back_pin': 27,
    },
    'right': {
        'speed_pin': 18,
        'forward_pin': 23,
        'back_pin': 24,
    },
    'camera': {
        'pin': 25,
    },
}

camera = {
    "port": 8000,
    "frame_rate": 15,
    "resolution": (480, 320),
    "quality": 15,
}
