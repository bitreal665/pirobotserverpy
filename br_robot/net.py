import socket
from br_logger import logger
from . import config


class Net:

    __tag = "NET"

    def __init__(self, executor, cmd_parser, a_life_cycle, a_logger):
        self.__logger = a_logger
        self.__life_cycle = a_life_cycle
        self.__cmd_parser = cmd_parser
        self.__executor = executor
        self.__conn = None
        self.__sock = None

    def init(self):
        self.__logger.log(logger.I, self.__tag, "initing...")
        try:
            self.__sock = socket.socket()
            self.__sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.__sock.bind(('', config.net['port']))
            self.__sock.listen(1)
            self.__logger.log(logger.I, self.__tag, "socket opened on %s port" % config.net['port'])
        except Exception as e:
            self.__logger.log(logger.E, self.__tag, "net exception (init):")
            self.__logger.log(logger.E, self.__tag, e)
            return False
        self.__logger.log(logger.I, self.__tag, "inited")
        return True

    def start(self):
        self.__logger.log(logger.I, self.__tag, "start loop")

        while not self.__life_cycle.is_exit():
            try:
                # accept
                self.__conn, addr = self.__sock.accept()
                self.__logger.log(logger.I, self.__tag, "----- %s accepted -----" % str(addr))

                while not self.__life_cycle.is_exit():
                    result = 0
                    cmd_result = 0
                    cmd_payload = ''

                    # read
                    req_str = self.__read()
                    if req_str == "":
                        self.__logger.log(logger.I, self.__tag, "req_str is empty")
                        break
                    cmd = self.__cmd_parser.parse(req_str)
                    if cmd is None:
                        result = 1

                    # exec
                    if cmd is not None:
                        result, cmd_result, cmd_payload = self.__executor.execute(cmd)

                    # write
                    resp_str = "%s %s %s" % (result, cmd_result, cmd_payload)
                    if self.__write(resp_str) == 0:
                        self.__logger.log(logger.I, self.__tag, "sent 0")
                        break

            except Exception as e:
                self.__logger.log(logger.W, self.__tag, "net exception (read/write):")
                self.__logger.log(logger.W, self.__tag, e)

            self.__logger.log(logger.I, self.__tag, "----- close connection -----")

            # shutdown
            try:
                self.__conn.shutdown(socket.SHUT_RDWR)
            except Exception as e:
                self.__logger.log(logger.W, self.__tag, "net exception (shutdown conn):")
                self.__logger.log(logger.W, self.__tag, e)

            # close
            try:
                self.__conn.close()
            except Exception as e:
                self.__logger.log(logger.W, self.__tag, "net exception (close conn):")
                self.__logger.log(logger.W, self.__tag, e)

            self.__conn = None

        self.__logger.log(logger.I, self.__tag, "stop loop")

    def __read(self):
        req_bytes = self.__conn.recv(1024)
        if len(req_bytes) >= 1024:
            self.__logger.log(logger.W, self.__tag, "too long req")
        req_str = req_bytes.decode("utf-8")
        self.__logger.log(logger.D, self.__tag, "read '%s' (clip_last_char:%s)" % (req_str, config.net['in']['clip_last_char']))
        if req_str == '':
            return req_str
        if config.net['in']['clip_last_char']:
            req_str = req_str[0:-1]
        return req_str

    def __write(self, resp_str):
        if config.net['out']['add_cr']:
            resp_str += '\n'
        self.__logger.log(logger.D, self.__tag, "write '%s' (add_cr:%s)" % (resp_str, config.net['out']['add_cr']))
        resp_bytes = bytearray(resp_str, 'utf-8')
        return self.__conn.send(resp_bytes)

    def deinit(self):
        self.__logger.log(logger.I, self.__tag, "deiniting...")
        # sock
        self.__logger.log(logger.I, self.__tag, "close socket")
        # close
        try:
            self.__sock.close()
        except Exception as e:
            self.__logger.log(logger.W, self.__tag, "net exception (close sock):")
            self.__logger.log(logger.W, self.__tag, e)
        self.__sock = None
        self.__logger.log(logger.I, self.__tag, "deinited")
