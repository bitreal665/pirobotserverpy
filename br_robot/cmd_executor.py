from br_logger import logger


class CmdExecutor:

    __tag = "CMD_EXEC"

    __cmds = {
        'ENABLE_CAMERA': (lambda robot, args: robot.enable_camera(int(args[0])), [int]),
        'DRIVE': (lambda robot, args: robot.drive(float(args[0]), float(args[1])), [float, float]),
        'ROTATE_CAMERA': (lambda robot, args: robot.rotate_camera(float(args[0])), [float]),
        'EXIT': (lambda robot, args: robot.exit(), []),
    }

    def __init__(self, interface, a_logger):
        self.__interface = interface
        self.__logger = a_logger

    def execute(self, cmd):

        self.__logger.log(logger.D, self.__tag, str(cmd))

        cmd_result = 0
        cmd_payload = ''

        if cmd['name'] not in self.__cmds:
            self.__logger.log(logger.W, self.__tag, "unknown cmd: %s" % cmd['name'])
            return 2, cmd_result, cmd_payload

        a_cmd = self.__cmds[cmd['name']]

        if len(cmd['args']) != len(a_cmd[1]):
            self.__logger.log(logger.W, self.__tag, "bad args count. %s need %s args. now args %s" % (cmd['name'], len(a_cmd[1]), len(cmd['args'])))
            return 3, cmd_result, cmd_payload

        try:
            self.__logger.log(logger.D, self.__tag, "start cmd execution")
            cmd_result, cmd_payload = a_cmd[0](self.__interface, cmd['args'])
        except ValueError as e:
            self.__logger.log(logger.W, self.__tag, "cmd arguments exception:")
            self.__logger.log(logger.W, self.__tag, e)
            return 4, cmd_result, cmd_payload
        except Exception as e:
            self.__logger.log(logger.W, self.__tag, "cmd execution exception:")
            self.__logger.log(logger.W, self.__tag, e)
            return 5, cmd_result, cmd_payload

        return 0, cmd_result, cmd_payload

