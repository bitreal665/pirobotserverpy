from . import config
from . import camera_service
from . import gpio_controller
from . import cmd_executor
from . import cmd_parser
from . import net
from . import robot_interface
from . import life_cycle
from base_app import BaseApp


class RobotService(BaseApp):

    __tag = 'ROBOT_SERVICE'

    def __init__(self):
        super(RobotService, self).__init__(RobotService.__tag, config.logger)
        self.__camera_service = camera_service.CameraService(self._logger)
        self._app_components.append(self.__camera_service)
        self.__gpio = gpio_controller.GPIOController(self._logger)
        self._app_components.append(self.__gpio)
        self.__life_cycle = life_cycle.LifeCycle(self._logger)
        self._app_components.append(self.__life_cycle)
        self.__interface = robot_interface.RobotInterface(self._logger, self.__camera_service, self.__gpio, self.__life_cycle)
        self._app_components.append(self.__interface)
        executor = cmd_executor.CmdExecutor(self.__interface, self._logger)
        a_cmd_parser = cmd_parser.CmdParser(self._logger)
        self.__net = net.Net(executor, a_cmd_parser, self.__life_cycle, self._logger)
        self._app_components.append(self.__net)

    def _on_main_loop(self):
        self.__net.start()
