from br_logger import *


class BaseApp:

    def __init__(self, tag, logger_settings):
        self.__tag = tag
        self._logger = composite_logger.CompositeLogger([
            console_logger.ConsoleLogger(logger_settings["console"]),
            file_logger.FileLogger(logger_settings["file"])
        ])
        self._app_components = []

    def start(self):
        if self.__init():
            self.__main_loop()
            self.__deinit()

    def __init(self):
        console_logger.log2console("init begin")
        if not self.__init_module(self._logger, None):
            return False
        for ac in self._app_components:
            if not self.__init_module(ac, self._logger):
                return False
        self._on_init()
        self._logger.log(logger.I, self.__tag, "init end")
        return True

    def _on_init(self):
        pass

    def __init_module(self, mod, used_logger):
        if not mod.init():
            if used_logger:
                used_logger.log(logger.E, self.__tag, str(mod) + " initing error")
            else:
                console_logger.log2console(str(mod) + "initing error")
            return False
        return True

    def __main_loop(self):
        self._logger.log(logger.I, self.__tag, "===== start begin =====")
        try:
            self._on_main_loop()
        except Exception as e:
            self.__logger.log(logger.E, self.__tag, "unhandled exception in main loop:")
            self.__logger.log(logger.E, self.__tag, e)
        self._logger.log(logger.I, self.__tag, "===== start end =====")

    def _on_main_loop(self):
        pass

    def __deinit(self):
        self._logger.log(logger.I, self.__tag, "deinit begin")
        self._on_deinit()
        for ac in self._app_components[::-1]:
            ac.deinit()
        self._logger.deinit()
        console_logger.log2console("deinit end")

    def _on_deinit(self):
        pass
